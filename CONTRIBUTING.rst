Contributing
============

Dependencies
------------


Tests/Developement (pip)::

- tox
- coverage
- unittest
- mock
- responses

Documentation (pip)::

- sphinx
- sphinxcontrib-napoleon
- sphinx_rtd_theme


Install
-------

install either with or without development dependencies::

    pip3 install poetry  # or: python3 -m pip install poetry
    poetry install --with dev,docs --compile

    or

    poetry install --without dev,docs --compile


Upgrade the dependencies (which are managed by poetry and listed in poetry.lock::

    poetry update

Then don't forget to add the updated poetry.lock to git.

Tests
-----

**Run** (from project root)::

`poetry run tox`

**Full Suite**::

  tox

This will run:

ToDo (frennkie): update this

or::

  make test
  # and to clean up after

Clean up::
  make clean

Building PyOTRS package: ``python setup.py sdist`` (``make build`` )

Uploading package to pypi (requires credentials): ``python setup.py sdist bdist_wheel upload`` (``make upload``)

Releasing
---------

- Update CHANGELOG.rst
- Update pyotrs/version.py
- Update pyproject.toml
- git add CHANGELOG.rst pyproject.toml pyotrs/version.py
- git commit -m "bump to 0.x.y"
- git tag 0.x.y
- git push
- git push --tags
- poetry build
- poetry publish

Writing Documentation
---------------------

Google Docstring format for::

- Args: (includes **kwargs)
- Returns:
- Raises:
- Examples:

Sphinx Napoleon Docs::

    https://sphinxcontrib-napoleon.readthedocs.org/en/latest/
    https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html#example-google

To including private method in the automatically generate Sphinx documentation add::

:private-members:


For a Spinx formatted "Note" or "Warning" use .rst Syntax:

- ``.. note::``
- ``.. warning::``

Some links::

    http://www.sphinx-doc.org/en/stable/markup/para.html
    http://www.sphinx-doc.org/en/stable/rest.html
    http://docutils.sourceforge.net/docs/user/rst/quickref.html#escaping
    https://gist.github.com/dupuy/1855764
